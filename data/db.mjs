import { LowSync, JSONFileSync } from 'lowdb';

const adapter = new JSONFileSync('data/db.json');
const db = new LowSync(adapter);
db.read();

export default db;
