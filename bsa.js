import Chat from './src/components/chat/Chat';
import rootReducer from './src/rootReducer';

export default { Chat, rootReducer };
