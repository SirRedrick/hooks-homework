export const ApiPath = {
  AUTH: '/auth',
  MESSAGES: '/messages',
  USERS: '/users'
};
