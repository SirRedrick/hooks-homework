import express, { Router } from 'express';
import path, { dirname } from 'path';
import cors from 'cors';
import passport from 'passport';
import LocalStrategy from 'passport-local';
import { initApi } from './api/api.mjs';
import { API_PATH } from './common/enums/enums.mjs';
import { users as usersRepository } from './repositories/repositories.mjs';
import { fileURLToPath } from 'url';

const app = express();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

passport.use(
  new LocalStrategy(function (username, password, done) {
    const user = usersRepository.getByName(username);

    if (!user) {
      return done(null, false, { message: 'Incorrect username.' });
    }
    if (!(user.password === password)) {
      return done(null, false, { message: 'Incorrect password.' });
    }

    return done(null, user);
  })
);
app.use(passport.initialize());

app.use(API_PATH, initApi(Router));

const __dirname = dirname(fileURLToPath(import.meta.url));
app.use(express.static(path.join(__dirname, './client/build')));

app.get('/*', (req, res) => {
  res.sendFile(path.join(__dirname, './client/build', 'index.html'));
});

const PORT = process.env.PORT || 3001;
app.listen(PORT, () => console.info(`Server listening port ${PORT}`));
