export class Users {
  constructor({ usersRepository }) {
    this._usersRepository = usersRepository;
  }

  getUsers() {
    return this._usersRepository.getAll();
  }

  getByName(name) {
    return this._usersRepository.getByName(name);
  }

  updateUser(id, changes) {
    return this._usersRepository.updateUser(id, changes);
  }

  deleteUser(id) {
    return this._usersRepository.delete(id);
  }
}
