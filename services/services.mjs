import {
  messages as messagesRepository,
  users as usersRepository
} from '../repositories/repositories.mjs';
// import { Auth } from './auth/auth.service.mjs';
import { Messages } from './messages/messages.service.mjs';
import { Users } from './users/users.service.mjs';

// export const auth = new Auth({
//   userRepository
// });

export const messages = new Messages({
  messagesRepository,
  usersRepository
});

export const users = new Users({
  usersRepository
});
