export class Messages {
  constructor({ messagesRepository, usersRepository }) {
    this._messagesRepository = messagesRepository;
    this._usersRepository = usersRepository;
  }

  getMessages() {
    return this._messagesRepository.getAll();
  }

  addMessage(text, userId) {
    const user = this._usersRepository.getById(userId);
    return this._messagesRepository.createMessage(text, user);
  }

  updateMessage(id, changes) {
    return this._messagesRepository.updateMessage(id, changes);
  }

  deleteMessage(id) {
    this._messagesRepository.delete(id);
  }
}
