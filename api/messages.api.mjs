import { MessagesApiPath } from '../common/enums/enums.mjs';

export const initMessages = (Router, services) => {
  const { messages: messagesService } = services;
  const router = Router();

  router
    .get(MessagesApiPath.ROOT, (req, res) => {
      const messages = messagesService.getMessages();
      res.send(messages);
    })
    .post(MessagesApiPath.ROOT, (req, res) => {
      const { text, userId } = req.body;
      const message = messagesService.addMessage(text, userId);
      res.json(message);
    })
    .put(MessagesApiPath.$ID, (req, res) => {
      const { id } = req.params;
      const changes = req.body;
      const message = messagesService.updateMessage(id, changes);
      res.json(message);
    })
    .delete(MessagesApiPath.$ID, (req, res) => {
      const { id } = req.params;
      messagesService.deleteMessage(id);
      res.status(201).send();
    });

  return router;
};
