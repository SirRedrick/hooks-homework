import { UsersApiPath } from '../common/enums/enums.mjs';

export const initUsers = (Router, services) => {
  const { users: usersService } = services;
  const router = Router();

  router
    .get(UsersApiPath.ROOT, (req, res) => {
      const users = usersService.getUsers();
      res.json(users);
    })
    .put(UsersApiPath.$ID, (req, res) => {
      const { id } = req.params;
      const changes = req.body;
      const updatedUser = usersService.updateUser(id, changes);
      res.json(updatedUser);
    })
    .delete(UsersApiPath.$ID, (req, res) => {
      const { id } = req.params;
      usersService.deleteUser(id);
      res.status(201).send();
    });

  return router;
};
