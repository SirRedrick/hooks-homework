import { ApiPath } from '../common/enums/enums.mjs';
import { messages, users } from '../services/services.mjs';
import { initAuth } from './auth.api.mjs';
import { initMessages } from './messages.api.mjs';
import { initUsers } from './users.api.mjs';

export const initApi = Router => {
  const apiRouter = Router();

  apiRouter.use(ApiPath.AUTH, initAuth(Router, { users }));
  apiRouter.use(ApiPath.MESSAGES, initMessages(Router, { messages }));
  apiRouter.use(ApiPath.USERS, initUsers(Router, { users }));

  return apiRouter;
};
