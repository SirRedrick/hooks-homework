import passport from 'passport';
import { AuthApiPath } from '../common/enums/enums.mjs';

export const initAuth = (Router, services) => {
  const { users: usersService } = services;
  const router = Router();

  router.post(AuthApiPath.LOGIN, passport.authenticate('local', { session: false }), (req, res) => {
    const { username } = req.body;

    const { id, avatar, name, role } = usersService.getByName(username);
    res.send({ id, avatar, name, role });
  });

  return router;
};
