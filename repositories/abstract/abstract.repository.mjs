import { v4 } from 'uuid';

export class Abstract {
  constructor(db, repoName) {
    this.dbContext = db.data[repoName];
    this.db = db;
  }

  generateId() {
    return v4();
  }

  getAll() {
    return this.dbContext;
  }

  getById(id) {
    return this.dbContext.find(it => it.id === id);
  }

  create(item) {
    const { id } = item;
    this.dbContext.push(item);
    this.db.write();
    return this.getById(id);
  }

  update(item) {
    const { id } = item;
    this.dbContext = this.dbContext.map(it => (it.id === id ? item : it));
    this.db.write();
    return this.getById(id);
  }

  delete(id) {
    this.dbContext = this.dbContext.filter(it => it.id !== id);
    this.db.write();
  }
}
