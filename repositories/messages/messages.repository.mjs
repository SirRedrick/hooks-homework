import { Abstract } from '../abstract/abstract.repository.mjs';

class Messages extends Abstract {
  constructor(db) {
    super(db, 'messages');
  }

  createMessage(text, _user) {
    const { id: userId, avatar, name: user } = _user;
    const id = this.generateId();
    const message = {
      id,
      userId,
      avatar,
      user,
      text,
      createdAt: new Date().toISOString(),
      editedAt: ''
    };

    return this.create(message);
  }

  updateMessage(id, changes) {
    let message = this.getById(id);
    message = {
      ...message,
      ...changes
    };
    message.editedAt = new Date().toISOString();
    return this.update(message);
  }
}

export { Messages };
