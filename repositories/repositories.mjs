import db from '../data/db.mjs';
import { Messages } from './messages/messages.repository.mjs';
import { Users } from './users/users.repository.mjs';

export const messages = new Messages(db);
export const users = new Users(db);
