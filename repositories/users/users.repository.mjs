import { Abstract } from '../abstract/abstract.repository.mjs';

class Users extends Abstract {
  constructor(db) {
    super(db, 'users');
  }

  getByName(name) {
    return this.dbContext.find(user => user.name === name);
  }

  createUser(name, password) {
    const id = this.generateId();
    const user = {
      id,
      avatar: null,
      name,
      password,
      role: 'user'
    };

    return this.create(user);
  }

  updateUser(id, changes) {
    let user = this.getById(id);
    user = {
      ...user,
      ...changes
    };
    return this.update(user);
  }
}

export { Users };
