import { DateTime } from 'luxon';
import { Message } from '../store/services/messages';

export const sortMessages = (messages: Message[]): Message[] => {
  return [...messages].sort(
    (messageA, messageB) =>
      DateTime.fromISO(messageA.createdAt).toMillis() -
      DateTime.fromISO(messageB.createdAt).toMillis()
  );
};
