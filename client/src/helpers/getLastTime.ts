import { DateTime } from 'luxon';
import { Message } from '../store/services/messages';

export const getLastTime = (messages: Message[]): string => {
  const dates = messages.map((message: Message) => DateTime.fromISO(message.createdAt));
  return DateTime.max(...dates)?.toFormat('dd.LL.yyyy HH:mm');
};
