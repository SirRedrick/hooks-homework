import { Message } from '../store/services/messages';

export const getUserCount = (messages: Message[]): number => {
  const userIds = messages.map((message: Message) => message.userId);
  return new Set(userIds).size;
};
