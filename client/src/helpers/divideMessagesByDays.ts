import { DateTime } from 'luxon';
import { Message } from '../store/services/messages';
import { sortMessages } from './helpers';

export const divideMessagesByDays = (messages: Message[]): Record<string, Message[]> => {
  const sortedMessages = sortMessages(messages);

  return sortedMessages.reduce<Record<string, Message[]>>((acc, message: Message) => {
    const date = DateTime.fromISO(message.createdAt);
    const displayDate = date.toFormat('cccc, d LLLL');
    const diff = Math.floor(DateTime.now().diff(date, ['days']).days);

    switch (diff) {
      case 0:
        return {
          ...acc,
          Today: acc.Today ? [...acc.Today, message] : [message]
        };
      case 1:
        return {
          ...acc,
          Yesterday: acc.Yesterday ? [...acc.Yesterday, message] : [message]
        };
      default:
        return {
          ...acc,
          [displayDate]: acc[displayDate] ? [...acc[displayDate], message] : [message]
        };
    }
  }, {});
};
