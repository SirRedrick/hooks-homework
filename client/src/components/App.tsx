import * as React from 'react';
import { Switch, Route } from 'react-router-dom';
import CssBaseline from '@material-ui/core/CssBaseline';
import { Chat } from './chat/Chat';
import { Login } from './login/Login';
import './App.css';
import { ProtectedRoute } from './common/ProtectedRoute';
import { MessageEditor } from './chat/components/components';
import { UserList } from './chat/components/UserList';
import { UserEditor } from './chat/components/UserEditor';
import { AdminRoute } from './common/AdminRoute';

function App() {
  return (
    <React.Fragment>
      <CssBaseline />
      <Switch>
        <Route exact path="/login" component={Login} />
        <ProtectedRoute exact path="/" component={Chat} />
        <ProtectedRoute path="/editMessage/:id" component={MessageEditor} />
        <AdminRoute path="/userList" component={UserList} />
        <AdminRoute path="/editUser/:id" component={UserEditor} />
      </Switch>
    </React.Fragment>
  );
}

export default App;
