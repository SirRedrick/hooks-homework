import * as React from 'react';
import {
  Container,
  Typography,
  Avatar,
  TextField,
  Button,
  CircularProgress
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { LockOutlined as LockOutlinedIcon } from '@material-ui/icons';
import { useLoginMutation, LoginRequest } from '../../store/services/auth';
import { useHistory } from 'react-router-dom';
import { useSnackbar } from 'notistack';

const useStyles = makeStyles(theme => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: '100%',
    marginTop: theme.spacing(1)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  },
  wrapper: {
    position: 'relative'
  },
  progress: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -8,
    marginLeft: -12
  }
}));

export const Login = () => {
  const [formState, setFormState] = React.useState<LoginRequest>({
    username: '',
    password: ''
  });
  const [login, { isLoading }] = useLoginMutation();
  const { push } = useHistory();
  const { enqueueSnackbar } = useSnackbar();
  const classes = useStyles();

  const handleChange = ({ target: { name, value } }: React.ChangeEvent<HTMLInputElement>) =>
    setFormState(prev => ({ ...prev, [name]: value }));

  const handleLogin = async (e: React.FormEvent<HTMLButtonElement>) => {
    e.preventDefault();

    try {
      const { role } = await login(formState).unwrap();
      if (role === 'user') {
        push('/');
      } else if (role === 'admin') {
        push('/userList');
      }
    } catch (err) {
      enqueueSnackbar('Login failed', { variant: 'error' });
    }
  };

  const areInputsInvalid = !Boolean(formState.username && formState.password);

  return (
    <Container component="main" maxWidth="xs">
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Log in
        </Typography>
        <form className={classes.form} noValidate>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            label="User name"
            name="username"
            autoFocus
            disabled={isLoading}
            onChange={handleChange}
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            label="Password"
            name="password"
            disabled={isLoading}
            onChange={handleChange}
          />
          <div className={classes.wrapper}>
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
              disabled={isLoading || areInputsInvalid}
              onClick={handleLogin}
            >
              Log in
            </Button>
            {isLoading && <CircularProgress size={24} className={classes.progress} />}
          </div>
        </form>
      </div>
    </Container>
  );
};
