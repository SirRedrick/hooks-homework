import * as React from 'react';
import { Container, CircularProgress } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { useSnackbar, VariantType } from 'notistack';
import { useGetMessagesQuery } from '../../store/services/messages';
import { Header, MessageList, MessageInput } from './components/components';
import { useGetUsersQuery } from '../../store/services/users';
import { useSelector } from 'react-redux';
import { selectCurrentUser } from '../../store/auth/authSlice';
import { useHistory } from 'react-router-dom';
import { sortMessages } from '../../helpers/sortMessages';

const useStyles = makeStyles({
  root: {
    height: '100vh'
  },
  container: {
    height: '100%',
    display: 'grid',
    gridTemplateRows: '88px minmax(0, 1fr) 88px'
  },
  messageList: {
    overflowY: 'auto'
  },
  wrapper: {
    display: 'grid',
    placeItems: 'center'
  }
});

export const Chat = () => {
  const { data: messages, isLoading, isError } = useGetMessagesQuery();
  const { data: users } = useGetUsersQuery();
  const user = useSelector(selectCurrentUser);
  const { enqueueSnackbar } = useSnackbar();
  const { push } = useHistory();
  const classes = useStyles();

  React.useEffect(() => {
    window.addEventListener('keydown', editLast);
    return () => {
      window.removeEventListener('keydown', editLast);
    };
  });

  const showSnackbar = (message: string, variant: VariantType) =>
    enqueueSnackbar(message, { variant });

  const editLast = (e: KeyboardEvent) => {
    if (e.key !== 'ArrowUp') return;

    if (messages && user) {
      const sortedMessages = sortMessages(messages);
      if (sortedMessages[messages.length - 1].userId === user.id)
        push(`/editMessage/${messages[messages.length - 1].id}`);
    }
  };

  return (
    <Container maxWidth="lg" classes={{ root: classes.root }} className="chat">
      <div className={classes.container}>
        <Header name={'Chat'} />
        {isLoading ? (
          <div className={classes.wrapper}>
            <CircularProgress />
          </div>
        ) : isError ? (
          <div className={classes.wrapper}>
            <span>Sorry, we couldn't get your messages</span>
          </div>
        ) : (
          <MessageList messages={messages} users={users} />
        )}
        <MessageInput showSnackbar={showSnackbar} />
      </div>
    </Container>
  );
};
