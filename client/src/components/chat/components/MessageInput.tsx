import * as React from 'react';
import { TextField, Button, Grid } from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';
import { useAddMessageMutation } from '../../../store/services/messages';
import { selectCurrentUser } from '../../../store/auth/authSlice';
import { useSelector } from 'react-redux';
import { VariantType } from 'notistack';
import { User } from '../../../store/services/auth';

const useStyles = makeStyles({
  form: {
    display: 'flex',
    alignItems: 'center'
  }
});

type Props = {
  showSnackbar: (text: string, variant: VariantType) => void;
};

export const MessageInput = ({ showSnackbar }: Props) => {
  const [text, setText] = React.useState('');
  const [error, setError] = React.useState(false);
  const { id: userId } = useSelector(selectCurrentUser) as User;
  const [addMessage] = useAddMessageMutation();

  const classes = useStyles();

  const isValid = () => {
    if (text === '') {
      setError(true);
      return false;
    }
    return true;
  };

  const onChange = (e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>): void => {
    setError(false);
    setText(e.currentTarget.value);
  };

  const handleClick = async (e: React.FormEvent<HTMLButtonElement>) => {
    e.preventDefault();
    if (!isValid()) return;

    try {
      await addMessage({ text, userId }).unwrap();
      setText('');
    } catch (err) {
      showSnackbar('Message could not be added', 'error');
    }
  };

  return (
    <form className={classes.form}>
      <Grid container spacing={2} alignItems="stretch">
        <Grid item xs={11}>
          <TextField
            error={error}
            helperText={error ? "Message can't be empty" : ''}
            size="small"
            label="Your message"
            multiline
            variant="outlined"
            fullWidth
            type="text"
            value={text}
            onBlur={isValid}
            onChange={onChange}
          />
        </Grid>
        <Grid item xs={1}>
          <Button type="submit" color="primary" variant="contained" onClick={handleClick}>
            Send
          </Button>
        </Grid>
      </Grid>
    </form>
  );
};
