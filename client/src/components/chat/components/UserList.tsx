import * as React from 'react';
import {
  List,
  ListItem,
  ListItemAvatar,
  Avatar,
  CircularProgress,
  ListItemText,
  ListItemSecondaryAction,
  IconButton,
  Tooltip,
  AppBar,
  Toolbar,
  Container,
  Link
} from '@material-ui/core';
import { Settings as SettingsIcon, Delete as DeleteIcon } from '@material-ui/icons';
import { makeStyles } from '@material-ui/core/styles';
import { Link as RouterLink } from 'react-router-dom';
import { useSnackbar } from 'notistack';
import { useDeleteUserMutation, useGetUsersQuery } from '../../../store/services/users';

const useStyles = makeStyles(theme => ({
  wrapper: {
    display: 'grid',
    placeItems: 'center'
  },
  link: {
    color: theme.palette.primary.contrastText
  }
}));

export const UserList = () => {
  const { data: users, isLoading, isError } = useGetUsersQuery();
  const [deleteUser] = useDeleteUserMutation();
  const { enqueueSnackbar } = useSnackbar();
  const classes = useStyles();

  const handleClick = async (id: string) => {
    try {
      await deleteUser(id).unwrap();
    } catch (err) {
      enqueueSnackbar('User deletion failed', { variant: 'error' });
    }
  };

  return (
    <Container maxWidth="lg">
      <AppBar position="static">
        <Toolbar>
          <Link className={classes.link} component={RouterLink} to="/">
            Back to chat
          </Link>
        </Toolbar>
      </AppBar>
      <List>
        {isLoading ? (
          <div className={classes.wrapper}>
            <CircularProgress />
          </div>
        ) : isError ? (
          <div className={classes.wrapper}>
            <span>Sorry, we couldn't get users</span>
          </div>
        ) : (
          users?.map(user => (
            <ListItem key={user.id}>
              <ListItemAvatar>
                <Avatar src={user.avatar} />
              </ListItemAvatar>
              <ListItemText primary={user.name} />
              <ListItemSecondaryAction>
                <IconButton component={RouterLink} to={`editUser/${user.id}`}>
                  <Tooltip title="Edit user" arrow>
                    <SettingsIcon />
                  </Tooltip>
                </IconButton>
                <IconButton onClick={() => handleClick(user.id)}>
                  <Tooltip title="Delete user" arrow>
                    <DeleteIcon />
                  </Tooltip>
                </IconButton>
              </ListItemSecondaryAction>
            </ListItem>
          ))
        )}
      </List>
    </Container>
  );
};
