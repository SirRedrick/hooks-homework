import * as React from 'react';
import { ListItem, ListItemText, Grid, IconButton } from '@material-ui/core';
import { Settings as SettingsIcon, Delete as DeleteIcon } from '@material-ui/icons';
import { makeStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';
import { useSnackbar } from 'notistack';
import { useDeleteMessageMutation } from '../../../store/services/messages';

const useStyles = makeStyles({
  root: {
    width: 'max-content',
    marginLeft: 'auto'
  },
  messageText: {
    flexGrow: 0
  }
});

type Props = {
  id: string;
  text: string | undefined;
  time: string;
  onEdit: () => void;
  onDelete: () => void;
};

export const OwnMessage = ({ id, text, time, onEdit, onDelete }: Props) => {
  const [deleteMessage] = useDeleteMessageMutation();
  const { enqueueSnackbar } = useSnackbar();
  const classes = useStyles();

  const handleClick = async () => {
    try {
      await deleteMessage(id).unwrap();
    } catch (err) {
      enqueueSnackbar('Message deletion failed', { variant: 'error' });
    }
  };

  return (
    <ListItem className="own-message" classes={{ root: classes.root }} alignItems="center">
      <ListItemText
        className={classes.messageText}
        primary={<small className="message-time">{time}</small>}
        secondaryTypographyProps={{ component: 'div' }}
        secondary={
          <Grid container>
            <Grid item xs={12}>
              <span className="message-text">{text}</span>
            </Grid>
            <Grid item xs={12}>
              <IconButton
                size="small"
                className="message-edit"
                component={Link}
                to={`editMessage/${id}`}
              >
                <SettingsIcon fontSize="small" />
              </IconButton>
              <IconButton size="small" className="message-delete" onClick={handleClick}>
                <DeleteIcon fontSize="small" />
              </IconButton>
            </Grid>
          </Grid>
        }
      />
    </ListItem>
  );
};
