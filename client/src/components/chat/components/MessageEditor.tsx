import * as React from 'react';
import {
  Container,
  Typography,
  TextField,
  Button,
  ButtonGroup,
  CircularProgress
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { Link, useHistory, useParams } from 'react-router-dom';
import { useSnackbar } from 'notistack';
import { useEditMessageMutation, useGetMessagesQuery } from '../../../store/services/messages';

const useStyles = makeStyles(theme => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: '100%',
    marginTop: theme.spacing(1)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  },
  wrapper: {
    position: 'relative'
  },
  progress: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -8,
    marginLeft: -12
  },
  link: {
    color: theme.palette.primary.contrastText,
    textDecoration: 'none'
  }
}));

export const MessageEditor = () => {
  const { id } = useParams<{ id: string }>();
  const { message } = useGetMessagesQuery(undefined, {
    selectFromResult: ({ data }) => ({ message: data?.find(message => message.id === id) })
  });
  const [text, setText] = React.useState(message?.text);
  const [edit, { isLoading }] = useEditMessageMutation();
  const { push } = useHistory();
  const { enqueueSnackbar } = useSnackbar();
  const classes = useStyles();

  const handleSubmit = async (e: React.FormEvent<HTMLButtonElement>) => {
    e.preventDefault();
    if (!text) return;

    try {
      await edit({ id, text }).unwrap();
      push('/');
    } catch (err) {
      enqueueSnackbar('Message editing failed', { variant: 'error' });
    }
  };

  return (
    <Container component="main" maxWidth="xs">
      <div className={classes.paper}>
        <Typography component="h1" variant="h5">
          Edit Message
        </Typography>
        <form className={classes.form} noValidate>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            multiline
            rows={4}
            label="Text"
            name="username"
            autoFocus
            value={text}
            disabled={isLoading}
            onChange={e => setText(e.target.value)}
          />
          <div className={classes.wrapper}>
            <ButtonGroup fullWidth disabled={isLoading}>
              <Button
                onClick={handleSubmit}
                type="submit"
                variant="contained"
                color="primary"
                className={classes.submit}
              >
                Confirm
              </Button>
              <Button
                variant="contained"
                color="primary"
                className={classes.submit}
                component={Link}
                to="/"
              >
                Cancel
              </Button>
              {false && <CircularProgress size={24} className={classes.progress} />}
            </ButtonGroup>
          </div>
        </form>
      </div>
    </Container>
  );
};
