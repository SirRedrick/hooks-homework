import * as React from 'react';
import { Toolbar, Typography, Tooltip, Link } from '@material-ui/core';
import { Person as PersonIcon, Chat as ChatIcon } from '@material-ui/icons';
import { makeStyles } from '@material-ui/core/styles';
import { Link as RouterLink } from 'react-router-dom';
import { useGetMessagesQuery } from '../../../store/services/messages';
import { getUserCount, getLastTime } from '../../../helpers/helpers';
import { useSelector } from 'react-redux';
import { selectCurrentUser } from '../../../store/auth/authSlice';

const useStyles = makeStyles({
  infoItem: {
    display: 'flex',
    alignItems: 'center',
    marginLeft: '24px'
  },
  infoItemIcon: {
    marginRight: '4px'
  },
  time: {
    marginLeft: 'auto'
  }
});

type Props = {
  name: string;
};

export const Header = ({ name }: Props) => {
  const { userCount, messageCount, lastTime } = useGetMessagesQuery(undefined, {
    selectFromResult: ({ data }) => ({
      userCount: data ? getUserCount(data) : 'N/A',
      messageCount: data ? data.length : 'N/A',
      lastTime: data ? getLastTime(data) : 'N/A'
    })
  });
  const user = useSelector(selectCurrentUser);
  const classes = useStyles();

  return (
    <Toolbar className="header">
      <Typography variant="h4" component="h1" className="header-title">
        {name}
      </Typography>
      <Typography className={classes.infoItem}>
        <Tooltip title="User count" arrow>
          <PersonIcon className={classes.infoItemIcon} />
        </Tooltip>
        <span className="header-users-count">{userCount}</span>
      </Typography>
      <Typography className={classes.infoItem}>
        <Tooltip title="Message count" arrow>
          <ChatIcon className={classes.infoItemIcon} />
        </Tooltip>
        <span className="header-messages-count">{messageCount}</span>
      </Typography>
      {user?.role === 'admin' && (
        <Link component={RouterLink} to="/userList">
          <Typography className={classes.infoItem}>User List</Typography>
        </Link>
      )}
      <Typography className={classes.time}>
        Last message at: <span className="header-last-message-date">{lastTime}</span>
      </Typography>
    </Toolbar>
  );
};
