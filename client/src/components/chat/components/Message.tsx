import * as React from 'react';
import {
  ListItem,
  ListItemAvatar,
  ListItemText,
  Avatar,
  IconButton,
  Grid
} from '@material-ui/core';
import { Favorite as FavoriteIcon, FavoriteBorder as FavoriteBorderIcon } from '@material-ui/icons';
import { makeStyles } from '@material-ui/core/styles';
import { useEditMessageMutation } from '../../../store/services/messages';
import { useSnackbar } from 'notistack';

const useStyles = makeStyles({
  root: {
    width: 'max-content',
    maxWidth: '100%'
  },
  messageText: {
    flexGrow: 0
  }
});

interface Props {
  id: string;
  name: string;
  text: string | undefined;
  time: string;
  avatar: string | undefined;
  isLiked: boolean;
}

export const Message = ({ id, name, text, time, avatar, isLiked }: Props) => {
  const [edit] = useEditMessageMutation();
  const classes = useStyles();
  const { enqueueSnackbar } = useSnackbar();

  const handleLike = async (e: React.MouseEvent<HTMLButtonElement>) => {
    e.preventDefault();

    try {
      await edit({ id, isLiked: !isLiked }).unwrap();
    } catch (err) {
      enqueueSnackbar('Message like failed', { variant: 'error' });
    }
  };

  return (
    <ListItem className="message" classes={{ root: classes.root }} alignItems="center">
      <ListItemAvatar>
        <Avatar src={avatar} alt="" className="message-user-avatar" />
      </ListItemAvatar>
      <ListItemText
        className={classes.messageText}
        primary={
          <React.Fragment>
            <span className="message-user-name">{name}</span>
            <small className="message-time">{time}</small>
          </React.Fragment>
        }
        secondaryTypographyProps={{ component: 'div' }}
        secondary={
          <Grid container>
            <Grid item xs={12}>
              <span className="message-text">{text}</span>
            </Grid>
            <Grid item xs={12}>
              <IconButton
                color="primary"
                size="small"
                className={isLiked ? 'message-liked' : 'message-like'}
                onClick={handleLike}
              >
                {isLiked ? (
                  <FavoriteIcon fontSize="small" />
                ) : (
                  <FavoriteBorderIcon fontSize="small" />
                )}
              </IconButton>
            </Grid>
          </Grid>
        }
      />
    </ListItem>
  );
};
