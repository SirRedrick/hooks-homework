import * as React from 'react';
import {
  Container,
  Typography,
  TextField,
  Button,
  ButtonGroup,
  CircularProgress
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { Link, useHistory, useParams } from 'react-router-dom';
import { useSnackbar } from 'notistack';
import { useEditUserMutation, useGetUsersQuery } from '../../../store/services/users';

const useStyles = makeStyles(theme => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center'
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: '100%',
    marginTop: theme.spacing(1)
  },
  submit: {
    margin: theme.spacing(3, 0, 2)
  },
  wrapper: {
    position: 'relative'
  },
  progress: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -8,
    marginLeft: -12
  },
  link: {
    color: theme.palette.primary.contrastText,
    textDecoration: 'none'
  }
}));

export const UserEditor = () => {
  const { id } = useParams<{ id: string }>();
  const { user } = useGetUsersQuery(undefined, {
    selectFromResult: ({ data }) => ({ user: data?.find(user => user.id === id) })
  });
  const [formState, setFormState] = React.useState({
    name: user?.name,
    avatar: user?.avatar
  });
  const [edit, { isLoading }] = useEditUserMutation();
  const { push } = useHistory();
  const { enqueueSnackbar } = useSnackbar();
  const classes = useStyles();

  const handleChange = ({ target: { name, value } }: React.ChangeEvent<HTMLInputElement>) =>
    setFormState(prev => ({ ...prev, [name]: value }));

  const handleSubmit = async (e: React.FormEvent<HTMLButtonElement>) => {
    e.preventDefault();
    if (!formState.name) return;

    try {
      await edit({ id, name: formState.name, avatar: formState.avatar }).unwrap();
      push('/');
    } catch (err) {
      enqueueSnackbar('Message editing failed', { variant: 'error' });
    }
  };

  return (
    <Container component="main" maxWidth="xs">
      <div className={classes.paper}>
        <Typography component="h1" variant="h5">
          Edit Message
        </Typography>
        <form className={classes.form} noValidate>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            label="Name"
            name="name"
            autoFocus
            value={formState.name}
            disabled={isLoading}
            onChange={handleChange}
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            label="Avatar"
            name="avatar"
            autoFocus
            value={formState.avatar}
            disabled={isLoading}
            onChange={handleChange}
          />
          <div className={classes.wrapper}>
            <ButtonGroup fullWidth disabled={isLoading}>
              <Button
                onClick={handleSubmit}
                type="submit"
                variant="contained"
                color="primary"
                className={classes.submit}
              >
                Confirm
              </Button>
              <Button
                variant="contained"
                color="primary"
                className={classes.submit}
                component={Link}
                to="/userList"
              >
                Cancel
              </Button>
              {false && <CircularProgress size={24} className={classes.progress} />}
            </ButtonGroup>
          </div>
        </form>
      </div>
    </Container>
  );
};
