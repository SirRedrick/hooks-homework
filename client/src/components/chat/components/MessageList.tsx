import * as React from 'react';
import { useSelector } from 'react-redux';
import { List, ListSubheader, Divider, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { Message, OwnMessage } from './components';
import { divideMessagesByDays } from '../../../helpers/helpers';
import { Message as IMessage } from '../../../store/services/messages';
import { selectCurrentUser } from '../../../store/auth/authSlice';
import { usePrevious } from '../../../hooks/use-previous';
import { User } from '../../../store/services/auth';
import { DateTime } from 'luxon';

const useStyles = makeStyles({
  listWrapper: {
    overflowY: 'auto'
  },
  ul: {
    padding: 0
  }
});

type Props = {
  messages: IMessage[] | undefined;
  users: User[] | undefined;
};

export const MessageList = ({ messages, users }: Props) => {
  const { id: userId } = useSelector(selectCurrentUser) as User;
  const classes = useStyles();
  const endRef = React.useRef<HTMLDivElement>(null);
  const prevMessages = usePrevious<IMessage[] | undefined>(messages);

  React.useEffect(() => {
    const receivedMessages = messages && !prevMessages;
    const messagesChanged = messages && prevMessages && messages.length !== prevMessages.length;

    if (receivedMessages || messagesChanged) {
      endRef.current?.scrollIntoView({ behavior: 'smooth' });
    }
  }, [messages, prevMessages]);

  let messageItems;
  if (messages) {
    const dividedMessages = divideMessagesByDays(messages);

    messageItems = Object.entries(dividedMessages).map(([key, value]) => {
      return (
        <li key={key}>
          <ul className={classes.ul}>
            <ListSubheader className="messages-divider">
              <Divider />
              <Typography>{key}</Typography>
            </ListSubheader>
            {value.map((message: IMessage) => {
              const { id, userId: messageUserId, text, createdAt, avatar, isLiked } = message;
              const user = users?.find(user => user.id === messageUserId);

              return userId === message.userId ? (
                <OwnMessage
                  key={id}
                  id={id}
                  text={text}
                  time={DateTime.fromISO(createdAt).toFormat('HH:mm')}
                  onEdit={() => {}}
                  onDelete={() => {}}
                />
              ) : (
                <Message
                  key={id}
                  id={id}
                  name={user ? user.name : 'Loading...'}
                  text={text}
                  time={DateTime.fromISO(createdAt).toFormat('HH:mm')}
                  avatar={avatar}
                  isLiked={isLiked}
                />
              );
            })}
          </ul>
        </li>
      );
    });
  }

  return (
    <div className={classes.listWrapper}>
      <List className="message-list" subheader={<li />}>
        {messageItems}
        <div ref={endRef} />
      </List>
    </div>
  );
};
