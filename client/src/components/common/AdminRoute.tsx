import { Redirect, Route, RouteProps } from 'react-router-dom';
import { useAuth } from '../../hooks/use-auth';

export const AdminRoute = ({ ...rest }: RouteProps) => {
  const user = useAuth();

  if (user && user.role === 'admin') {
    return <Route {...rest} />;
  } else {
    return <Redirect to={{ pathname: '/login' }} />;
  }
};
