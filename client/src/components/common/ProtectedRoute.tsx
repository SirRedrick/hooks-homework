import { Redirect, Route, RouteProps } from 'react-router-dom';
import { useAuth } from '../../hooks/use-auth';

export const ProtectedRoute = ({ ...rest }: RouteProps) => {
  const user = useAuth();

  if (user) {
    return <Route {...rest} />;
  } else {
    return <Redirect to={{ pathname: '/login' }} />;
  }
};
