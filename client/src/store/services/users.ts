import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';

export interface User {
  id: string;
  avatar: string;
  name: string;
  role: string;
}

export const usersApi = createApi({
  reducerPath: 'usersApi',
  baseQuery: fetchBaseQuery({ baseUrl: '/api/' }),
  tagTypes: ['Users'],
  endpoints: builder => ({
    getUsers: builder.query<User[], void>({
      query: () => 'users',
      providesTags: ['Users']
    }),
    addUser: builder.mutation<
      User,
      {
        name: string;
        password: string;
      }
    >({
      query: body => ({
        url: 'users',
        method: 'POST',
        body
      }),
      invalidatesTags: ['Users']
    }),
    editUser: builder.mutation<User, Partial<User>>({
      query: data => {
        const { id, ...body } = data;
        return {
          url: `users/${id}`,
          method: 'PUT',
          body
        };
      },
      invalidatesTags: ['Users']
    }),
    deleteUser: builder.mutation<void, string>({
      query: id => ({
        url: `users/${id}`,
        method: 'DELETE'
      }),
      invalidatesTags: ['Users']
    })
  })
});

export const { useGetUsersQuery, useAddUserMutation, useEditUserMutation, useDeleteUserMutation } =
  usersApi;
