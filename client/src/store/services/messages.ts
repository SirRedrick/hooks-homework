import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
export interface Message {
  id: string;
  userId: string;
  avatar: string;
  user: string;
  text: string;
  createdAt: string;
  editedAt: string;
  isLiked: boolean;
}

export const messagesApi = createApi({
  reducerPath: 'messagesApi',
  baseQuery: fetchBaseQuery({ baseUrl: '/api/' }),
  tagTypes: ['Messages'],
  endpoints: builder => ({
    getMessages: builder.query<Message[], void>({
      query: () => 'messages',
      providesTags: ['Messages']
    }),
    addMessage: builder.mutation<
      Message,
      {
        text: string;
        userId: string;
      }
    >({
      query: body => ({
        url: 'messages',
        method: 'POST',
        body
      }),
      invalidatesTags: ['Messages']
    }),
    editMessage: builder.mutation<Message, Partial<Message>>({
      query: data => {
        const { id, ...body } = data;
        return {
          url: `messages/${id}`,
          method: 'PUT',
          body
        };
      },
      invalidatesTags: ['Messages']
    }),
    deleteMessage: builder.mutation<void, string>({
      query: id => ({
        url: `messages/${id}`,
        method: 'DELETE'
      }),
      invalidatesTags: ['Messages']
    })
  })
});

export const {
  useGetMessagesQuery,
  useAddMessageMutation,
  useEditMessageMutation,
  useDeleteMessageMutation
} = messagesApi;
