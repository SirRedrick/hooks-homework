import { createSlice } from '@reduxjs/toolkit';
import { authApi, User } from '../services/auth';
import type { RootState } from '../store';

type AuthState = {
  user: User | null;
};

const initialState: AuthState = {
  user: null
};

const slice = createSlice({
  name: 'auth',
  initialState,
  reducers: {},
  extraReducers: builder => {
    builder.addMatcher(authApi.endpoints.login.matchFulfilled, (state, { payload }) => {
      state.user = payload;
    });
  }
});

export default slice.reducer;

export const selectCurrentUser = (state: RootState) => state.auth.user;
